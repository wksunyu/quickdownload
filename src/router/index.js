import Vue from 'vue'
import Router from 'vue-router'
import Device from '@/components/device/device'
import PCDevice from "@/components/pc/pc.vue"
import History from 'html5-history-api'
Vue.use(Router)
Vue.use(History)
const router = new Router({
  mode: 'history',
  base :'../../dist',
  linkActiveClass: 'active',
  routes: [
    {
      path: '/quickdownload/dist/down',
      component: Device
    },
    {
      path: '/quickdownload/dist/pcdown',
      component: PCDevice
    },
    {
      path: '/miguvip/down/quickdownload/dist/down',
      component:Device
    },
    {
      path: '/miguvip/down/quickdownload/dist/pcdown',
      component:PCDevice
    }
  ]
})
export  default router
