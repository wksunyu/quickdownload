/**
 * Created by gladyu on 17/9/12.
 */
/**
 * Created by gladyu on 16/9/7.
 */
var channel = 'wktv';
var browser = {
  versions: function () {
    var u = navigator.userAgent, app = navigator.appVersion;
    return {//移动终端浏览器版本信息
      iOS: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
      android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1,
      isWeixin: u.toLowerCase().match(/MicroMessenger/i) == "micromessenger",
      isQQ: u.toLowerCase().indexOf('mqqbrowser') > -1 && u.toLowerCase().indexOf('qq/') > -1,
      isWeibo: u.toLowerCase().indexOf('weibo') > -1,
    };
  }()
};
window.sendCNZZ = function (preStr, eventKey) {
  if (typeof _czc != 'undefined') {
    var sys = browser.versions.iOS ? 'ios' : 'android';
    _czc.push(['_trackEvent', sys + preStr, eventKey]);
  }
}
var otherChannel = {
  "wkrecmd2":'',
  "wkhelp":''
}
exports.downloadApp = function () {
  var time = new Date().getTime();
  channel = ((/down\?c\=(\S+)/).test(window.location.href)) ?
    window.location.href.match(/down\?c\=(\S+)/)[1].split('&')[0] : 'wktv';
  sendCNZZ('快点下载页', channel);
  //特殊逻辑
  if (channel in otherChannel && browser.versions.android) {
   window.location.href  = 'wukongtv://download?adUrl=https://down1.wukongtv.com/quickcast/app-' + channel + "-release.apk?"+ time +"&adDownName=quickcast&adStatName=quickcast";
  }else{
    //正常逻辑
    if (browser.versions.iOS && !browser.versions.isWeibo) {
      window.location.href = "https://itunes.apple.com/app/apple-store/id1276104080?pt=118763999&ct=" + channel + "&mt=8"
    } else {
      if (browser.versions.isWeixin || browser.versions.isQQ || browser.versions.isWeibo) {
        $("#float-div").show();
        $("#guide-android").show();
      } else {
        window.location.href = "https://down1.wukongtv.com/quickcast/app-" + channel + '-release.apk?'+ time;
      }
    }
  }
}
$('#float-div,#guide-android').on('click', function () {
  $("#float-div").hide();
  $("#guide-android").hide();
})
